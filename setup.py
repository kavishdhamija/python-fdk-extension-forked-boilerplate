"""Library Metadata Information."""

from setuptools import find_packages
from setuptools import setup

description = ('Fork to sanic extension boilerplate for fynd')
long_description = ""

setup(
    name='sanic_boilerplate',
    version='0.1',
    author='baymax',
    author_email='kavishdhamija@gofynd.com',
    description=description,
    long_description=description,
    url='https://github.com/gofynd/fdk-client-python',
    packages=find_packages(
        exclude=('tests*', 'documentation', '_macros')),
    license='',
    install_requires=[
        'structlog',
    ],
    classifiers=[
        'Programming Language :: Python :: 3.8.2'
    ],
)
