# cluster constants
FYND_CLUSTER = "https://api.fynd.com"

# access modes
OFFLINE_ACCESS_MODE = "offline"
ONLINE_ACCESS_MODE = "online"

SESSION_COOKIE_NAME = "ext_session"

SESSION_EXPIRY_IN_SECONDS = 900

ASSOCIATION_CRITERIA = {
    "ALL": "ALL",
    "SPECIFIC": "SPECIFIC-EVENTS"
}

TEST_WEBHOOK_EVENT_NAME = "ping"
